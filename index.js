console.log('script loaded');

const btn = document.querySelector('#myButton');
const input = document.querySelector('#myInput');
const xbtn = document.querySelector('#xButton');

btn.addEventListener('click', getDayOfTheWeek);
xbtn.addEventListener('click', clear);

const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

function getDayOfTheWeek() {
    const value = input.value;
    const regexp = /^(.{2}[\.\-\/].{2}[\.\-\/].{4})$/;
    const valid = !!value.match(regexp);

    if (!valid) {
        alert('value is not correct');
        return;
    }
    const splitted = value.split(/[\.\-\/]/);
    const date = new Date(splitted[2], +splitted[1] - 1, splitted[0]);
    alert(days[date.getDay() - 1]);
}

function clear() {
    const value = input.value;
    if (value.length > 0) {
        input.value = '';
    } else {
        input.value = 'undefined';
    }
}